package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class Vessel {

    public Vessel(PositiveInteger shieldHP,
                  PositiveInteger hullHP,
                  PositiveInteger powerGridOutput,
                  PositiveInteger capacitorAmount,
                  PositiveInteger capacitorRechargeRate,
                  PositiveInteger speed,
                  PositiveInteger size,
                  AttackSubsystem attackSubsystem,
                  DefenciveSubsystem defenciveSubsystem) {

    }

    private CombatReadyShip combatReadyShip;
    private DockedShip dockedShip;

    public CombatReadyShip getCombatReadyShip() {
        return this.combatReadyShip;
    }

    public DockedShip getDockedShip() {
        return this.dockedShip;
    }

    public CombatReadyShip getCombatReady() {
        return this.combatReadyShip;
    }
}
