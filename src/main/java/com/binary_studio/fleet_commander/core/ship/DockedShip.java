package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {



	public static DockedShip construct(String name,
									   PositiveInteger shieldHP,
									   PositiveInteger hullHP,
									   PositiveInteger powergridOutput,
									   PositiveInteger capacitorAmount,
									   PositiveInteger capacitorRechargeRate,
									   PositiveInteger speed,
									   PositiveInteger size) {
		if (name == null | name.trim().equals("")) {
			throw new IllegalArgumentException("Name should be not null and not Empty");
		}
		return new DockedShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount, capacitorRechargeRate, speed, size);
	}

	private final String name;
	private final PositiveInteger shieldHP;
	private final PositiveInteger hullHP;
	private PositiveInteger powergridOutput;
	private final PositiveInteger capacitorAmount;
	private final PositiveInteger capacitorRechargeRate;
	private final PositiveInteger speed;
	private final PositiveInteger size;
	private AttackSubsystem attackSubsystem;
	private DefenciveSubsystem defenciveSubsystem;
	private Vessel vessel;

	public DockedShip(String name,
					  PositiveInteger shieldHP,
					  PositiveInteger hullHP,
					  PositiveInteger powergridOutput,
					  PositiveInteger capacitorAmount,
					  PositiveInteger capacitorRechargeRate,
					  PositiveInteger speed,
					  PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.powergridOutput = powergridOutput;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null & this.attackSubsystem != null)
		{
			this.powergridOutput = PositiveInteger.of
					(this.powergridOutput.value() + this.attackSubsystem.getPowerGridConsumption().value());
		}
		if (this.powergridOutput.compareTo(subsystem.getPowerGridConsumption()) >= 0) {
			this.powergridOutput = PositiveInteger.of
					(this.powergridOutput.value() - subsystem.getPowerGridConsumption().value());
		}
		else {
			throw new InsufficientPowergridException
					(subsystem.getPowerGridConsumption().value() - this.powergridOutput.value());
		}
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null & this.defenciveSubsystem != null) {
			this.powergridOutput = PositiveInteger.of
					(this.powergridOutput.value() + this.defenciveSubsystem.getPowerGridConsumption().value());
		}
		if (this.powergridOutput.compareTo(subsystem.getPowerGridConsumption()) >= 0) {
			this.powergridOutput = PositiveInteger.of
					(this.powergridOutput.value() - subsystem.getPowerGridConsumption().value());
			this.defenciveSubsystem = subsystem;
		}
		else {
			throw new InsufficientPowergridException
					(subsystem.getPowerGridConsumption().value() - this.powergridOutput.value());
		}
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
			if (this.attackSubsystem == null) {
				throw  NotAllSubsystemsFitted.attackMissing();
			}
			if (this.defenciveSubsystem == null){
				throw NotAllSubsystemsFitted.defenciveMissing();
			}
			this.vessel = new Vessel(this.shieldHP, this.hullHP, this.powergridOutput, this.capacitorAmount,
					this.capacitorRechargeRate, this.speed, this.size, this.attackSubsystem, this.defenciveSubsystem);
		return this.vessel.getCombatReady();
	}

}
