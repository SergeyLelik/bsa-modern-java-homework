package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	public static DefenciveSubsystemImpl construct(String name,
												   PositiveInteger powergridConsumption,
												   PositiveInteger capacitorConsumption,
												   PositiveInteger impactReductionPercent,
												   PositiveInteger shieldRegeneration,
												   PositiveInteger hullRegeneration)
			throws IllegalArgumentException {
		if (name == null | name.trim().equals("")) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		return new DefenciveSubsystemImpl(name,
				powergridConsumption,
				capacitorConsumption,
				impactReductionPercent,
				shieldRegeneration,
				hullRegeneration);
	}

	private final String name;
	private final PositiveInteger powerGridConsumption;
	private final PositiveInteger capacitorConsumption;
	private final PositiveInteger impactReductionPercent;
	private final PositiveInteger shieldRegeneration;
	private final PositiveInteger hullRegeneration;

	public DefenciveSubsystemImpl(String name,
								  PositiveInteger powerGridConsumption,
								  PositiveInteger capacitorConsumption,
								  PositiveInteger impactReductionPercent,
								  PositiveInteger shieldRegeneration,
								  PositiveInteger hullRegeneration) {
		this.name = name;
		this.powerGridConsumption = powerGridConsumption;
		this.capacitorConsumption = capacitorConsumption;
		this.impactReductionPercent = impactReductionPercent;
		this.shieldRegeneration = shieldRegeneration;
		this.hullRegeneration = hullRegeneration;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powerGridConsumption;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		return new AttackAction(PositiveInteger.of(
				(int) Math.ceil(incomingDamage.damage.value() - incomingDamage.damage.value() *
						(double) (this.impactReductionPercent.value()) / 100)),
				incomingDamage.attacker, incomingDamage.target, incomingDamage.weapon);
	}

	@Override
	public RegenerateAction regenerate() {
		return new RegenerateAction(this.shieldRegeneration, this.hullRegeneration);
	}

}
