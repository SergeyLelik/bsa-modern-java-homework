package com.binary_studio.dependency_detector;

import java.util.ArrayList;
import java.util.Arrays;

public final class DependencyDetector {

	private DependencyDetector() {
	}

	public static boolean canBuild(DependencyList libraries) {
		if (libraries.dependencies.size() == 0 | libraries.dependencies.size() == 1) {
			return true;
		} else {
			ArrayList<String> listKey = new ArrayList<>();
			ArrayList<String> listVal = new ArrayList<>();
			listKey.add(Arrays.asList(libraries.dependencies.get(0)).get(0));
			listVal.add(Arrays.asList(libraries.dependencies.get(0)).get(1));
			for (int i = 1; i < libraries.dependencies.size(); i++){
				for (int j = 0; j < listKey.size(); j++) {
					if (listVal.get(j).equals(Arrays.asList(libraries.dependencies.get(i)).get(0)) &
							listKey.get(j).equals(Arrays.asList(libraries.dependencies.get(i)).get(1))){
						return false;
					} else if (listVal.get(j).equals(Arrays.asList(libraries.dependencies.get(i)).get(0))) {
						listVal.set(j, Arrays.asList(libraries.dependencies.get(i)).get(1));
					} else {
						listKey.add(Arrays.asList(libraries.dependencies.get(i)).get(0));
						listVal.add(Arrays.asList(libraries.dependencies.get(i)).get(1));
						break;
					}
				}
			}
		}
		return true;

	}

//	public static boolean canBuild(DependencyList libraries) {
//		if (libraries.dependencies.size() == 0 | libraries.dependencies.size() == 1) {
//			return true;
//		} else {
//			ArrayList<String> listKey = new ArrayList<>();
//			ArrayList<String> listVal = new ArrayList<>();
//			listKey.add(Arrays.asList(libraries.dependencies.get(0)).get(0));
//			listVal.add(Arrays.asList(libraries.dependencies.get(0)).get(1));
//			for (int i = 1; i < libraries.dependencies.size(); i++){
//				for (int j = 0; j < listKey.size(); j++) {
//					if (listVal.get(i - 1).equals(Arrays.asList(libraries.dependencies.get(i)).get(0))){
//						return false;
//					} else if (listVal.get(j).equals(Arrays.asList(libraries.dependencies.get(i)).get(0))) {
//						listVal.set(j, Arrays.asList(libraries.dependencies.get(i)).get(1));
//					} else {
//						listKey.add(Arrays.asList(libraries.dependencies.get(i)).get(0));
//						listVal.add(Arrays.asList(libraries.dependencies.get(i)).get(1));
//						break;
//					}
//				}
//			}
//		}
//		return true;
//
//	}

}
