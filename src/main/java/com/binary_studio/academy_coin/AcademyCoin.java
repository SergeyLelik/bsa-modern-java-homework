package com.binary_studio.academy_coin;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class AcademyCoin {

	private AcademyCoin() {
	}

	public static int maxProfit(Stream<Integer> prices) {
		int result = 0;
		boolean flag = true;
		List<Integer> list = prices.collect(Collectors.toList());
		for (int i = 0; i < list.size() - 1 ; i++) {
			int bufferNum = i;
			if (list.get(i) < list.get(i + 1) & flag) {
				for (int j = i; j < list.size() - 1; j++) {
					if (list.get(j) < list.get(j + 1)){
						i = j;
					} else {
						i = j - 1;
						break;}
				}
				result = result + list.get(i + 1) - list.get(bufferNum);
				flag = false;
			} else {
				flag = true;
			}
		}
		return result;
	}

}
